#!/usr/bin/env bash

. hdfs-env.sh

HADOOP_VERSION="2.8.5"
POD_NAME="hdfs"
sudo podman pod kill ${POD_NAME}
sudo podman pod rm -f ${POD_NAME}
sudo podman pod create --name ${POD_NAME} \
        -p 8020:8020 \
        -p 50070:50070 \
        -p 50010:50010 \
        -p 50011:50011 \
        -p 50012:50012 \
        -p 50020:50020 \
        -p 50021:50021 \
        -p 50022:50022 \
        -p 50080:50080 \
        -p 50081:50081 \
        -p 50082:50082 \

export NN_OPTS="$DN_NN_COMMON_OPTS \
                -Ddfs.namenode.rpc-address=0.0.0.0:8020 \
                -Ddfs.namenode.http-address=0.0.0.0:50070 \
                -Ddfs.namenode.resource.du.reserved=5368709120 \
                -Ddfs.namenode.replication.min=1 \
                -Ddfs.namenode.replication.max=1 \
                -Ddfs.namenode.handler.count=20 \
                -Ddfs.namenode.safemode.threshold-pct=1 \
                -Ddfs.namenode.safemode.min.datanodes=1 \
                -Ddfs.namenode.resource.checked.volumes.minimum=1 \
                -Ddfs.namenode.safemode.extension=1000 \
                -Ddfs.namenode.support.allow.format=true \
                -Ddfs.namenode.avoid.read.stale.datanode=true \
                -Ddfs.namenode.avoid.write.stale.datanode=true \
                -Ddfs.webhdfs.enabled=true \
                -Ddfs.image.compress=true \
                -Ddfs.image.compression.codec=org.apache.hadoop.io.compress.SnappyCodec"

sudo podman create \
        --name namenode \
        --hostname localhost \
        --pod ${POD_NAME} \
        -e CHECK_OR_RUN_HDFS_FORMAT=true \
        ${HADOOP_IMAGE_NAME} \
        ${NN_OPTS} \
        namenode

#export SN_OPTS="-Ddfs.namenode.rpc-address=localhost:8020 \
#                -Ddfs.namenode.secondary.http-address=0.0.0.0:50090 \
#                -Ddfs.namenode.checkpoint.max-retries=10 \
#                -Ddfs.namenode.checkpoint.period=1800 \
#                -Ddfs.namenode.checkpoint.txns=100000 \
#                -Ddfs.namenode.checkpoint.check.period=180 \
#                -Ddfs.namenode.num.checkpoints.retained=3"
#
#sudo podman create \
#        --name secnamenode \
#        --hostname localhost \
#        --pod ${POD_NAME} \
#        ${HADOOP_IMAGE_NAME} \
#        ${SN_OPTS} \
#        secondarynamenode

for ID in 0 1 2
do
    export DN_OPTS="-Ddfs.namenode.rpc-address=localhost:8020 \
                    $DN_NN_COMMON_OPTS \
                    -Ddfs.datanode.address=0.0.0.0:5001${ID} \
                    -Ddfs.datanode.http.address=0.0.0.0:5008${ID} \
                    -Ddfs.datanode.ipc.address=0.0.0.0:5002${ID} \
                    -Ddfs.heartbeat.interval=3 \
                    -Ddfs.datanode.du.reserved=1073741824 \
                    -Ddfs.datanode.du.reserved=1073741824 \
                    -Ddfs.datanode.balance.bandwidthpersec=1048576 \
                    -Ddfs.datanode.directoryscan.threads=2 \
                    -Ddfs.datanode.failed.volumes.tolerated=0"
    sudo podman create \
            --name datanode-${ID} \
            --hostname localhost \
            --pod ${POD_NAME} \
            ${HADOOP_IMAGE_NAME} \
            ${DN_OPTS} \
            datanode
done

sudo podman pod start ${POD_NAME}
sudo podman logs -f namenode