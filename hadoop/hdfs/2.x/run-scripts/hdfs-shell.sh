#!/usr/bin/env bash

HADOOP_VERSION=$1
. hdfs-env.sh

docker container run \
        -it \
        --rm \
        --net weave \
        $IMAGE \
        dfs \
        -fs hdfs://namenode:8020 \
        ${@}
