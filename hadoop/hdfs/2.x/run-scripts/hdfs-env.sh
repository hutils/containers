#!/usr/bin/env bash

HADOOP_VERSION="${HADOOP_VERSION:-2.8.5}"

REGISTRY="hutils"
HADOOP_IMAGE_NAME="$REGISTRY/hadoop-hdfs:$HADOOP_VERSION"

export DN_NN_COMMON_OPTS="-Ddfs.blocksize=128m \
                          -Ddfs.replication=1 \
                          -Ddfs.replication.max=1"
