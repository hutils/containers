#!/usr/bin/env bash

HADOOP_VERSION=$1
. hdfs-env.sh

docker container run \
        -it \
        --rm \
        --entrypoint="/bin/bash" \
        ${HADOOP_IMAGE_NAME} \
        /usr/local/hadoop-${HADOOP_VERSION}/bin/hadoop \
        checknative \
        -a
