#!/usr/bin/env bash

HADOOP_VERSION=$1
. ./hdfs-env.sh

cd ..

sudo podman build --no-cache --build-arg HADOOP_VERSION=${HADOOP_VERSION} -t ${HADOOP_IMAGE_NAME} .
if [[ $? != 0 ]]; then
    exit $?
fi
sudo podman push ${HADOOP_IMAGE_NAME}