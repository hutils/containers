#!/usr/bin/env bash

export STD_OPTS="-XX:MaxMetaspaceSize=$HADOOP_METASPACESIZE \
                 $JVM_JIT_OPTS \
                 $JVM_PERF_OPTS \
                 $HADOOP_GC_OPTS \
                 $JVM_MEMORY_OPTS \
                 $HBASE_TRACE_OPTS \
                 $HBASE_GC_TRACE_OPTS \
                 $HADOOP_JMX_BASE \
                 $JVM_CUSTOM_OPTS"

export HADOOP_NAMENODE_OPTS="$STD_OPTS"
export HADOOP_DATANODE_OPTS="$STD_OPTS"
export HADOOP_ZKFC_OPTS="$STD_OPTS"
export HADOOP_JOURNALNODE_OPTS="$STD_OPTS"
export HADOOP_SECONDARYNAMENODE_OPTS="$STD_OPTS"

umask 0022

export HADOOP_ROOT_LOGGER="$HADOOP_ROOT_LOGGER_LEVEL,$HADOOP_ROOT_LOGGER_APPENDER"
export HADOOP_SECURITY_LOGGER="$HADOOP_SECURITY_LOGGER_LEVEL,$HADOOP_SECURITY_LOGGER_APPENDER"
export HDFS_AUDIT_LOGGER="$HDFS_AUDIT_LOGGER_LEVEL,$HDFS_AUDIT_LOGGER_APPENDER"

CONF_PARAMS=""
ARGS=""
for key in $@; do
    if [[ ${key} == -D* ]]; then
        CONF_PARAMS="$CONF_PARAMS $key"
    else
        ARGS="$ARGS $key"
    fi
done

${JAVA_HOME}/bin/java --source 11 ${CONF_PARAMS} /PrepareHdfs.java
if [[ $? != 0 ]]; then
    exit $?
fi


if [ ${CHECK_OR_RUN_HDFS_FORMAT} = 'true' ]
then
    #if non interactive mode return code 1, then it find existing FS image and don't format it
    ${HADOOP_HOME}/bin/hdfs namenode -format -nonInteractive
fi

exec ${HADOOP_HOME}/bin/hdfs ${ARGS}