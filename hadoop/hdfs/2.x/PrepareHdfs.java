import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

public class PrepareHdfs {

  public static void main(String[] args) {
    var properties = System.getProperties();
    setDefaults(properties);

    for (Path confPath : List.of(HdfsEnv.hdfsSite, HdfsEnv.coreSite)) {
      try {
        genConfigFile(confPath, properties);
      }
      catch (Exception e) {
        fatalError("Can't create " + confPath + ". " + e.getMessage());
      }
    }
  }

  private static void genConfigFile(Path confPath, Properties properties) throws IOException {
    if (Files.exists(confPath)) {
      System.out.println(confPath + " already exists. Skipping creation.");
      return;
    }

    try (var configWriter = Files.newBufferedWriter(confPath, StandardOpenOption.CREATE,
                                                    StandardOpenOption.WRITE)) {

      configWriter.write("<?xml version=\"1.0\"?>");
      configWriter.newLine();
      configWriter.write("<configuration>");
      configWriter.newLine();
      final Pattern filterPattern = Pattern.compile(
          "^(java\\.|user\\.|path\\.|file\\.|sun\\.|line\\.|jdk\\.|os\\.|awt\\.).*");
      for (var kv : properties.entrySet()) {
        if (filterPattern.matcher(kv.getKey().toString()).matches()) {
          continue;
        }
        writeString(kv.getKey().toString(), kv.getValue().toString(), configWriter);
      }
      configWriter.write("</configuration>");
    }

    printFile(confPath);
  }

  private static void setDefaults(Properties properties) {
    properties.setProperty("dfs.namenode.name.dir",
                           Paths.get(System.getenv("DFS_NAME_DIR")).toString());
    properties.setProperty("dfs.namenode.edits.dir",
                           Paths.get(System.getenv("DFS_NAME_EDITS_DIR")).toString());
    properties.setProperty("dfs.namenode.checkpoint.dir",
                           Paths.get(System.getenv("DFS_SEC_NAME_DIR")).toString());
    properties.setProperty("dfs.namenode.checkpoint.edits.dir",
                           Paths.get(System.getenv("DFS_SEC_NAME_EDITS_DIR")).toString());
    properties.setProperty("dfs.datanode.data.dir",
                           Paths.get(System.getenv("DFS_DATA_DIR")).toString());
    properties.setProperty("dfs.datanode.max.transfer.threads", "4096");
  }

  private static void writeString(String confKey, String value, BufferedWriter configWriter)
      throws IOException {

    configWriter.write("<property>");
    configWriter.newLine();
    configWriter.write("\t<name>");
    configWriter.write(confKey);
    configWriter.write("</name>");
    configWriter.newLine();
    configWriter.write("\t<value>");
    configWriter.write(value);
    configWriter.write("</value>");
    configWriter.newLine();
    configWriter.write("</property>");
    configWriter.newLine();
  }

  private static void printFile(Path conf) throws IOException {
    System.out.println(conf + " file:"
                       + System.lineSeparator()
                       + StandardCharsets.UTF_8.decode(ByteBuffer.wrap(Files.readAllBytes(conf))));
  }

  private static void fatalError(String error) {
    System.err.println("FATAL: " + error);
    System.exit(-1);
  }

  static class HdfsEnv {

    public static final Path confDir;
    public static final Path hdfsSite;
    public static final Path coreSite;

    static {
      confDir = Paths.get(System.getenv("HADOOP_CONF_DIR"));
      hdfsSite = confDir.resolve("hdfs-site.xml");
      coreSite = confDir.resolve("core-site.xml");
    }

  }
}