FROM hutils/openjdk-shenandoah:11

ARG HADOOP_VERSION

#----read only vars----
ENV HDFS_USER="hdfs" \
    HADOOP_GROUP="hadoop" \
    BASE_INSTALL_PATH="/usr/local" \
    HADOOP_HOME="/usr/local/hadoop-$HADOOP_VERSION" \
    DFS_BASE_DIR="/var/lib/hadoop/hdfs" \
    HADOOP_LOG_DIR="/var/log/hadoop"
ENV DFS_NAME_DIR="$DFS_BASE_DIR/fs-image" \
    DFS_NAME_EDITS_DIR="$DFS_BASE_DIR/fs-image/edits" \
    DFS_SEC_NAME_DIR="$DFS_BASE_DIR/secondary/fs-image" \
    DFS_SEC_NAME_EDITS_DIR="$DFS_BASE_DIR/secondary/fs-image/edits" \
    DFS_DATA_DIR="$DFS_BASE_DIR/data" \
    HADOOP_CONF_DIR="/etc/hadoop" \
    HADOOP_LIBNATIVE_DIR="$HADOOP_HOME/lib/native" \
    HADOOP_UNIX_SOCKET_DIR="/var/run/hdfs" \
#customizable variables
    HADOOP_ROOT_LOGGER_LEVEL="INFO" \
    HADOOP_ROOT_LOGGER_APPENDER="console" \
    HADOOP_SECURITY_LOGGER_LEVEL="INFO" \
    HADOOP_SECURITY_LOGGER_APPENDER="NullAppender" \
    HDFS_AUDIT_LOGGER_LEVEL="INFO" \
    HDFS_AUDIT_LOGGER_APPENDER="NullAppender" \
#common JVM opts shared by all Hadoop binaries
#if 'true' then force namenode image dir format
    CHECK_OR_RUN_HDFS_FORMAT="false" \
    JVM_CUSTOM_OPTS="" \
    HADOOP_JMX_BASE="-Dcom.sun.management.jmxremote \
                     -Dcom.sun.management.jmxremote.local.only=false \
                     -Dcom.sun.management.jmxremote.ssl=false \
                     -Dcom.sun.management.jmxremote.authenticate=false" \
    HADOOP_OPTS="-Djava.net.preferIPv4Stack=true -Dnetworkaddress.cache.negative.ttl=0" \
    HADOOP_TRACE_OPTS="-XX:ErrorFile=$HADOOP_LOG_DIR/hs_err.log" \
    HADOOP_GC_TRACE_OPTS="-Xlog:gc=debug,gc+ergo=debug,gc+stats=debug:file=$HADOOP_LOG_DIR/gc.log:utctime,level,tags:filesize=10M,filecount=10" \
    HADOOP_GC_OPTS="-XX:+UnlockExperimentalVMOptions -XX:+UseShenandoahGC -XX:+DisableExplicitGC -XX:-UseBiasedLocking" \
    JVM_MEMORY_OPTS="-XX:+AlwaysPreTouch -XX:+UseLargePages -XX:+UseNUMA" \
    JVM_JIT_OPTS="-server" \
    JVM_PERF_OPTS="-XX:-PerfDataSaveToFile -XX:+PerfDisableSharedMem" \
    HADOOP_HEAPSIZE="512" \
    HADOOP_METASPACESIZE="96M" \
    HADOOP_CLIENT_OPTS="" \
    HADOOP_BALANCER_OPTS="" \
    HADOOP_MOVER_OPTS="" \
    HADOOP_PORTMAP_OPTS="" \
    HADOOP_NFS3_OPTS=""

#----image build----
ADD "http://apache-mirror.rbc.ru/pub/apache/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz" \
    $BASE_INSTALL_PATH/hadoop-$HADOOP_VERSION.tar.gz
ADD "https://bintray.com/hutils/hadoop/download_file?file_path=hadoop-native-${HADOOP_VERSION}.tar.gz" \
    $BASE_INSTALL_PATH/hadoop-native-${HADOOP_VERSION}.tar.gz

RUN groupadd --gid 15000 $HADOOP_GROUP \
    && useradd --uid 15000 -m -G $HADOOP_GROUP $HDFS_USER \
    && tar -xvzf $BASE_INSTALL_PATH/hadoop-${HADOOP_VERSION}.tar.gz -C $BASE_INSTALL_PATH \
    && rm -Rf $BASE_INSTALL_PATH/hadoop-${HADOOP_VERSION}.tar.gz \
              $HADOOP_LIBNATIVE_DIR/* \
    && tar -xvzf $BASE_INSTALL_PATH/hadoop-native-${HADOOP_VERSION}.tar.gz -C $HADOOP_LIBNATIVE_DIR \
    && rm -Rf $BASE_INSTALL_PATH/hadoop-native-${HADOOP_VERSION}.tar.gz \
              $HADOOP_HOME/share/doc \
              $HADOOP_HOME/include \
              $HADOOP_HOME/etc/hadoop \
    && ln -s $HADOOP_HOME/etc $HADOOP_CONF_DIR \
    && mkdir -p $HADOOP_UNIX_SOCKET_DIR \
                $HADOOP_LOG_DIR \
                /tmp/hadoop-$HDFS_USER \
                $DFS_NAME_DIR \
                $DFS_SEC_NAME_EDITS_DIR \
                $DFS_DATA_DIR \
                $DFS_SEC_NAME_DIR \
    && chmod -R 755 \
                $HADOOP_HOME \
                $HADOOP_LOG_DIR \
    && chmod -R 750 \
                $DFS_NAME_DIR \
                $DFS_SEC_NAME_EDITS_DIR \
                $DFS_DATA_DIR \
                $DFS_SEC_NAME_DIR \
    && chown -R $HDFS_USER:$HADOOP_GROUP \
                $HADOOP_HOME \
                $HADOOP_UNIX_SOCKET_DIR \
                $HADOOP_LOG_DIR \
                $HADOOP_CONF_DIR \
                /tmp/hadoop-$HDFS_USER \
                $DFS_NAME_DIR \
                $DFS_SEC_NAME_EDITS_DIR \
                $DFS_DATA_DIR \
                $DFS_SEC_NAME_DIR \
    && apt-get update \
    && apt-get install -y \
               libsnappy-dev \
               zlib1g \
               bzip2 \
               liblzo2-2 \
               libssl-dev \
    && apt-get clean -y

COPY --chown=hdfs:hadoop conf/log4j.properties $HADOOP_HOME/etc/
COPY --chown=hdfs:hadoop entrypoint.sh PrepareHdfs.java /

VOLUME $DFS_NAME_DIR \
       $DFS_DATA_DIR \
       $DFS_SEC_NAME_DIR \
       $DFS_SEC_NAME_EDITS_DIR \
       $HADOOP_HOME/etc \
       $HADOOP_UNIX_SOCKET_DIR \
       $HADOOP_LOG_DIR
EXPOSE 8020 50090 50091 50010 50075 50020 50070 50475 50470 50100 50105

WORKDIR $HADOOP_HOME/bin
USER $HDFS_USER:$HADOOP_GROUP
ENTRYPOINT ["/entrypoint.sh"]
CMD ["version"]
