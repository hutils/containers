#!/usr/bin/env bash

VERSION=$1

git clone ${GIT_URL}
cd hadoop
git checkout "branch-$VERSION"

#build distribution with native dependencies
#to build Hadoop native libs with support of all compression codecs:
#see https://hadoop.apache.org/docs/r2.8.3/hadoop-project-dist/hadoop-common/NativeLibraries.html
#1. sudo apt-get install gsasl cmake autoconf automake libtool libssl1.0-dev
#zlib1g-dev liblzo2-dev libbz2-dev libsnappy-dev bzip2
#2. mvn clean package -Pdist,native -DskipTests -Dtar
#3. see hadoop-dist/target/hadoop-$VERSION/lib/native
#4. check native lib loading using "/bin/hadoop checknative -a"
mvn clean package -Pdist,native -DskipTests -Dtar -Dfindbugs.skip=true -Dcheckstyle.skip=true -Drat.skip=true
RET_CODE=$?
if [[ ${RET_CODE} != 0 ]]; then
    exit ${RET_CODE}
fi

cp hadoop-dist/target/hadoop-${VERSION}.tar.gz ${BUILD_OUTPUT}/hadoop-${VERSION}.tar.gz

#pack native libs
cd hadoop-dist/target/hadoop-${VERSION}/lib/native
tar -czf ${BUILD_OUTPUT}/hadoop-native-${VERSION}.tar.gz lib*

echo "Hadoop build dir content:"
ls -l ${BUILD_OUTPUT}/*
chmod 777 -R ${BUILD_OUTPUT}