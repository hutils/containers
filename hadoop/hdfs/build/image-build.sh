#!/usr/bin/env bash

BUILD_VERSION=$1
. ./env.sh

sudo podman build --no-cache -t ${IMAGE_NAME} .
sudo podman push ${IMAGE_NAME}