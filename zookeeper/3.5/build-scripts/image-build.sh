#!/usr/bin/env bash

BUILD_VERSION=$1
. ./env.sh

cd ..

sudo podman build --no-cache --build-arg ZK_VERSION=$BUILD_VERSION -t $ZK_IMAGE_NAME .
if [[ $? != 0 ]]; then
    exit $?
fi
sudo podman push $ZK_IMAGE_NAME