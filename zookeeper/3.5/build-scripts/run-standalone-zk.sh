#!/usr/bin/env bash

. env.sh

sudo podman rm -f zk-1

# check server ID extraction from hostname
sudo podman create \
        --name zk-1 \
        --hostname zk-1 \
        -p 2181:2181 \
        $ZK_IMAGE_NAME \
        server

sudo podman start zk-1
sudo podman attach zk-1

sudo podman rm -f zk-2

# check server ID extraction env var
sudo podman create \
        --name zk-2 \
        -p 2181:2181 \
        -e ZK_SERVER_ID=1 \
        $ZK_IMAGE_NAME \
        server

sudo podman start zk-2
sudo podman attach zk-2

sudo podman rm -f zk-3
# check dynamic server config
sudo podman create \
        --name zk-3 \
        --hostname zk-3 \
        -e ZK_NODE_1="zk-3:2182:2183:participant;2181" \
        -p 2181:2181 \
        $ZK_IMAGE_NAME \
        server

sudo podman start zk-3
sudo podman attach zk-3