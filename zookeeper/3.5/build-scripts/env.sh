#!/usr/bin/env bash

REGISTRY="hutils"
BUILD_VERSION="${BUILD_VERSION:-3.5.4-beta}"
ZK_IMAGE_NAME="$REGISTRY/zookeeper:$BUILD_VERSION"
