#!/usr/bin/env bash

java -cp $ZK_INSTALLATION_DIR/*:$ZK_INSTALLATION_DIR/lib/* \
        org.apache.zookeeper.server.SnapshotFormatter \
        $ZK_DATA_LOG_DIR/$DATA_VERSION/$1
