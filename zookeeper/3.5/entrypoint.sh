#!/usr/bin/env bash

# TODO: join_to_cluster()
#    try:
#        if int(environ['RECONFIG_DELAY']) <= 0:
#            return
#    except:
#        return
#
#    sleep(int(environ['RECONFIG_DELAY']))
#
#    zk_cli_script = path.join(environ['ZK_INSTALLATION_DIR'], 'bin/zkCli.sh')
#    cur_node_record = read_node_list()[int(environ['ZK_SERVER_ID'])]
#    zk_cli_process = subprocess.Popen(['/bin/bash', zk_cli_script,
#                                       "reconfig",
#                                       "-add",
#                                       cur_node_record])
#    zk_cli_process.wait()
#    if zk_cli_process.returncode != 0:
#        exit(os.EX_TEMPFAIL)

if [[ $1 == "server" ]]; then
    ${JAVA_HOME}/bin/java --source 11 /PrepareZk.java
    if [[ $? != 0 ]]; then
        exit $?
    fi
    ZK_SCRIPT='zkServer.sh'
    shift
    ZK_ARGS="start-foreground $@"
elif [[ $1 == "txLog" ]]; then
    ZK_SCRIPT='tx-log.sh'
    shift
    ZK_ARGS=$@
elif [[ $1 == "snapshot" ]]; then
    ZK_SCRIPT='snapshot.sh'
    shift
    ZK_ARGS=$@
elif [[ $1 == "zkTxnLogToolkit" ]]; then
    ZK_SCRIPT='zkTxnLogToolkit.sh'
    shift
    ZK_ARGS=$@
else
    ZK_SCRIPT='zkCli.sh'
    shift
    ZK_ARGS=$@
fi

exec ${ZOOBINDIR}/${ZK_SCRIPT} ${ZK_ARGS}