import java.io.BufferedWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrepareZk {

  public static void main(String[] args) {
    try {
      generateMyIdFile();
    }
    catch (Exception e) {
      fatalError("Can't write myid file(" + ZkEnv.myid + "): " + e.getMessage());
    }

    try {
      genDynamicConfigFile();
    }
    catch (Exception e) {
      fatalError("Can't create " + ZkEnv.zooCfgDynamic + ". " + e.getMessage());
    }

    try {
      genZooCfgFile();
    }
    catch (Exception e) {
      fatalError("Can't create " + ZkEnv.zooCfg + ". " + e.getMessage());
    }
  }

  private static void generateMyIdFile() throws IOException {
    final byte serverId = getServerId();
    Files.writeString(ZkEnv.myid, serverId + "",
                      StandardOpenOption.TRUNCATE_EXISTING,
                      StandardOpenOption.WRITE);
    System.out.println("Server myid is " + serverId);
  }

  private static void genDynamicConfigFile() throws IOException {
    if (Files.exists(ZkEnv.zooCfgDynamic)) {
      System.out.println(ZkEnv.zooCfgDynamic + " already exists. Skipping creation.");
      printFile(ZkEnv.zooCfgDynamic);
      return;
    }

    final Pattern zkNodePattern = Pattern.compile("(ZK_NODE_)(\\d+)");
    try (var configWriter = Files.newBufferedWriter(ZkEnv.zooCfgDynamic,
                                                    StandardOpenOption.CREATE,
                                                    StandardOpenOption.WRITE)) {
      boolean noMatches = true;
      for (var kv : System.getenv().entrySet()) {
        final Matcher matcher = zkNodePattern.matcher(kv.getKey());
        if (matcher.matches()) {
          configWriter.write("server." + matcher.group(2) + "=");
          configWriter.write(kv.getValue());
          configWriter.newLine();
          noMatches = false;
        }
      }

      if (noMatches) {
        System.out.println("Dynamic config records not found in environment variables("
                           + "searching vars of format 'ZK_NODE_[1-254]'). "
                           + "Generate self record for this instance.");
        int zkServerId = getServerId();
        int zkClientPort = 2181;
        try {
          zkClientPort = Integer.parseInt(System.getenv("CONF_CLIENT_PORT"));
        }
        catch (NumberFormatException e) {}

        final String zkClientAddress = System.getenv("CONF_CLIENT_PORT_ADDRESS") != null
                                          ? System.getenv("CONF_CLIENT_PORT_ADDRESS")
                                          : "0.0.0.0";

        configWriter.write("server." + zkServerId + "="
                           + zkClientAddress + ":2182:2183:participant;" + zkClientPort);
        configWriter.newLine();
      }
    }

    printFile(ZkEnv.zooCfgDynamic);
  }

  private static void genZooCfgFile() throws IOException {
    if (Files.exists(ZkEnv.zooCfg)) {
      System.out.println("zoo.cfg already exists in " + ZkEnv.zooCfg + ". Skipping creation.");
      return;
    }

    try (var configWriter = Files.newBufferedWriter(ZkEnv.zooCfg, StandardOpenOption.CREATE,
                                                                  StandardOpenOption.WRITE)) {

      configureDirs(configWriter);
      configureQuorum(configWriter);
      configureTickTime(configWriter);
      configureSessions(configWriter);
      configureNetwork(configWriter);
      configureAdminServer(configWriter);
    }

    printFile(ZkEnv.zooCfg);
  }

  private static void printFile(Path zooCfg) throws IOException {
    System.out.println(zooCfg + " file:"
                       + System.lineSeparator()
                       + StandardCharsets.UTF_8.decode(ByteBuffer.wrap(Files.readAllBytes(zooCfg))));
  }

  private static void configureNetwork(BufferedWriter configWriter) throws IOException {
    writeInteger("globalOutstandingLimit", "CONF_GLOBAL_OUTSTANDING_LIMIT", 1000, configWriter);
    writeInteger("maxClientCnxns", "CONF_MAX_CLIENT_CNXNS", 0, configWriter);
    writeInteger("cnxTimeout", "CONF_CNX_TIMEOUT", 5, configWriter);
    writeString("quorumListenOnAllIPs", "CONF_QUORUM_LISTEN_ON_ALL_IPS", "false", configWriter);
  }

  private static void configureSessions(BufferedWriter configWriter) throws IOException {
    writeInteger("minSessionTimeout", "CONF_MIN_SESSION_TIMEOUT", 4000, configWriter);
    writeInteger("maxSessionTimeout", "CONF_MAX_SESSION_TIMEOUT", 40000, configWriter);
    writeString("localSessionsEnabled", "CONF_LOCAL_SESSIONS_ENABLED", "true", configWriter);
    writeString("localSessionsUpgradingEnabled", "CONF_LOCAL_SESSIONS_UPGRADING_ENABLED",
                 "true", configWriter);
  }

  private static void configureTickTime(BufferedWriter configWriter) throws IOException {
    writeInteger("tickTime", "CONF_TICK_TIME", 2000, configWriter);
    writeInteger("initLimit", "CONF_INIT_LIMIT", 5, configWriter);
    writeInteger("syncLimit", "CONF_SYNC_LIMIT", 10, configWriter);
  }

  private static void configureQuorum(BufferedWriter configWriter) throws IOException {
    writeInteger("electionAlg", "CONF_ELECTION_ALG", 3, configWriter);
    writeString("leaderServes", "CONF_LEADER_SERVES", "yes", configWriter);
    // always distributed mode because we always generate dynamic config file content
    writeString("standaloneEnabled", "false", configWriter);
    writeString("reconfigEnabled", "CONF_RECONFIG_ENABLED", "true", configWriter);
  }

  private static void configureAdminServer(BufferedWriter configWriter) throws IOException {
    writeString("admin.enableServer", "CONF_ADMIN_SERVER_ENABLED", "false", configWriter);
    writeString("admin.serverAddress", "CONF_ZK_ADMIN_SERVER", "0.0.0.0", configWriter);
    writeString("admin.serverPort", "CONF_ZK_ADMIN_PORT", "8080", configWriter);
    writeString("admin.commandURL", "CONF_ZK_COMMAND_URL", "/commands", configWriter);
  }

  private static void configureDirs(BufferedWriter configWriter) throws IOException {
    writeString("dataDir", ZkEnv.dataDir.toString(), configWriter);
    writeString("dataLogDir", ZkEnv.txLogDir.toString(), configWriter);
    writeString("dynamicConfigFile", ZkEnv.zooCfgDynamic.toString(), configWriter);
    writeInteger("preAllocSize", "CONF_PREALLOC_SIZE", 65535, configWriter);
    writeInteger("snapCount", "CONF_SNAPSHOT_COUNT", 100000, configWriter);
    writeInteger("autopurge.snapRetainCount", "CONF_AUTOPURGE_SNAP_RETAIN_COUNT", 3, configWriter);
    writeInteger("autopurge.purgeInterval", "CONF_AUTOPURGE_PURGE_INTERVAL", 2, configWriter);
    writeString("syncEnabled", "CONF_SYNC_ENABLED", "true", configWriter);
    writeString("forceSync", "CONF_FORCE_SYNC", "yes", configWriter);
  }

  private static void writeInteger(String confKey,
                                   String envKey,
                                   int defaultValue,
                                   BufferedWriter configWriter)
      throws IOException {

    configWriter.write(confKey + "=");
    int value = defaultValue;
    try {
      value = Integer.parseInt(System.getenv(envKey));
    }
    catch (Exception e) {}

    configWriter.write(Integer.toString(value));
    configWriter.newLine();
  }

  private static void writeString(String confKey,
                                  String envKey,
                                  String defaultValue,
                                  BufferedWriter configWriter)
      throws IOException {

    configWriter.write(confKey + "=");
    final String envVal = System.getenv(envKey);
    configWriter.write(envVal != null
                        ? envVal
                        : defaultValue);
    configWriter.newLine();
  }

  private static void writeString(String confKey, String value, BufferedWriter configWriter)
      throws IOException {

    configWriter.write(confKey + "=");
    configWriter.write(value);
    configWriter.newLine();
  }

  private static byte getServerId() throws UnknownHostException {
    byte zkServerId;
    var serverIdRaw = System.getenv("ZK_SERVER_ID");
    try {
      if (serverIdRaw != null && !serverIdRaw.isBlank()) {
        System.out.println("Extracting Zookeeper server ID from env variable ZK_SERVER_ID...");
        zkServerId = Byte.parseByte(serverIdRaw);
        if (zkServerId == 0) {
          fatalError("Zookeeper server ID(from env variable ZK_SERVER_ID) has invalid value 0");
        }
      }
      else {
        System.out.println("Extracting Zookeeper server ID from hostname...");
        zkServerId = getServerIdFromHostname();
      }
      return zkServerId;
    }
    catch (NumberFormatException e) {
      fatalError("Server ID must be have value (0-254], found value = " + serverIdRaw
                 + ". Please set ZK_SERVER_ID env variable explicitly or "
                 + "set hostname in format 'hostname-`SERVER_ID`[.mydomain.com]'");
      throw new RuntimeException("Must not be reached");
    }
  }

  private static byte getServerIdFromHostname() throws UnknownHostException {
    byte zkServerId;
    final String hostName = InetAddress.getLocalHost().getHostName();
    // extract ZK server ID from hostname(Kubernetes StatefulSet
    // use format "hostname-N.[...].cluster.local" or in simple format "hostname-N")
    final Pattern hostPattern = Pattern.compile("(.*-)(\\d)(\\..+)*");
    final Matcher matcher = hostPattern.matcher(hostName);
    if (!matcher.matches()) {
      fatalError("Can't extract server ID from hostname=" + hostName
                 + ". Please set ZK_SERVER_ID env variable explicitly or "
                 + "set hostname in format 'hostname-`SERVER_ID`.mydomain.com'");
    }

    zkServerId = Byte.parseByte(matcher.group(2));
    System.out.println("Found ID: " + zkServerId);
    if (zkServerId < 0) {
      fatalError("Kubernetes Statefulset has invalid indexing " + zkServerId);
    }
    zkServerId++;
    return zkServerId;
  }

  private static void fatalError(String error) {
    System.err.println("FATAL: " + error);
    System.exit(-1);
  }

  static class ZkEnv {

    public static final Path dataDir;
    public static final Path confDir;
    public static final Path txLogDir;
    public static final Path installDir;
    public static final String zkUser;
    public static final String zkGroup;
    public static final Path zooCfg;
    public static final Path zooCfgDynamic;
    public static final Path myid;

    static {
      dataDir = Paths.get(System.getenv("ZK_DATA_DIR"));
      txLogDir = Paths.get(System.getenv("ZK_DATA_LOG_DIR"));
      confDir = Paths.get(System.getenv("ZK_CONF_DIR"));
      installDir = Paths.get(System.getenv("ZK_INSTALLATION_DIR"));
      zkUser = System.getenv("ZK_USER");
      zkGroup = System.getenv("ZK_GROUP");
      zooCfg = confDir.resolve("zoo.cfg");
      zooCfgDynamic = confDir.resolve("zoo.cfg.dynamic");
      myid = dataDir.resolve("myid");
    }
  }
}