#!/usr/bin/env bash

JMX_DOCKER_FLAGS="-Dcom.sun.management.jmxremote.rmi.port=$JMXRMIPORT \
                  -Djava.rmi.server.hostname=$JMXRMIHOSTNAME \
                  -Dsun.rmi.transport.tcp.responseTimeout=$JMXRMITIMEOUT"

SERVER_JVMFLAGS="-Dzookeeper.datadir.autocreate=false \
                 -Dzookeeper.logger.level=$ZK_LOG_LEVEL \
                 -Dzookeeper.serverCnxnFactory=org.apache.zookeeper.server.NettyServerCnxnFactory \
                 -Dzookeeper.nio.numSelectorThreads=$CONF_NIO_NUM_SELECTOR_THREADS \
                 -Dzookeeper.nio.numWorkerThreads=$CONF_NUM_WORKER_THREADS \
                 -Dzookeeper.commitProcessor.numWorkerThreads=$CONF_COMMIT_PROCESSOR_NUM_WORKER_THREADS \
                 -Dfsync.warningthresholdms=$CONF_FSYNC_THRESHOLD \
                 -Dreadonlymode.enabled=$CONF_READONLYMODE_ENABLED \
                 -Dzookeeper.skipACL=$CONF_SKIP_ACL \
                 -Dznode.container.checkIntervalMs=$CONF_ZNODE_CONTAINER_CHECK_INTERVAL_MS \
                 -Dznode.container.maxPerMinute=$CONF_ZNODE_CONTAINER_MAX_PER_MINUTE \
                 -Dzookeeper.jmx.log4j.disable=true \
                 -Djava.net.preferIPv4Stack=true \
                 -Dnetworkaddress.cache.negative.ttl=0 \
                 $JMX_DOCKER_FLAGS \
                 -Xmx${ZK_SERVER_MAX_HEAP} \
                 -Xms${ZK_SERVER_MIN_HEAP} \
                 -XX:MaxDirectMemorySize=${ZK_SERVER_DIRECT_MEMORY} \
                 -XX:MaxMetaspaceSize=${ZK_SERVER_METASPACE} \
                 $JVM_MEMORY_OPTS \
                 $JVM_GC_OPTS \
                 $JVM_DIAG_OPTS \
                 $JVM_JIT_OPTS \
                 $JVM_PERF_OPTS \
                 $JVM_CUSTOM_OPTS"
