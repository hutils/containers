#!/usr/bin/env bash

. env.sh

#-v /home/lagrang/Documents/zk-tmp:/usr/local/zookeeper-3.5.4-beta/tmp \

docker container run \
        --name zk-1 \
        --hostname zk-1 \
        --network weave \
        -it \
        --rm \
        -p 2181:2181 \
        -p 8080:8080 \
        -e ZK_SERVER_ID=1 \
        $IMAGE_NAME
