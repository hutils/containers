#!/usr/bin/env bash

. env.sh

docker container run \
        --name zk-2 \
        --hostname zk-2 \
        --network weave \
        -it \
        --rm \
        -p 2182:2181 \
        -p 8081:8080 \
        -e ZK_SERVER_ID=2 \
        -e ZK_NODE_1="zk-1:2182:2183:participant;2181" \
        -e ZK_NODE_2="zk-2:2182:2183:participant;2181" \
        -e ZK_NODE_3="zk-3:2182:2183:participant;2181" \
        $IMAGE_NAME
