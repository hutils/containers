#!/usr/bin/env bash

. env.sh

docker container run \
        --name zk-3 \
        --hostname zk-3 \
        --network weave \
        -it \
        --rm \
        -p 2183:2181 \
        -p 8082:8080 \
        -e ZK_SERVER_ID=3 \
        -e ZK_NODE_1="zk-1:2182:2183:participant;2181" \
        -e ZK_NODE_2="zk-2:2182:2183:participant;2181" \
        -e ZK_NODE_3="zk-3:2182:2183:participant;2181" \
        $IMAGE_NAME
