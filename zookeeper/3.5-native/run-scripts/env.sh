#!/usr/bin/env bash

VERSION="3.5"
IMAGE_NAME="registry.gitlab.com/i-knowledge/dev-ops/zookeeper-native:$VERSION"
