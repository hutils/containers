#!/usr/bin/env bash

. env.sh

#-v ~/zookeeper-data/log:/var/log/zookeeper \
#-v ~/zookeeper-data/data/:/var/lib/zookeeper/data \
#-v ~/zookeeper-data/data/tx-log:/var/lib/zookeeper/data/tx-log \

docker container run \
        --name zk-1 \
        --hostname zk-1 \
        --network weave \
        -it \
        --rm \
        -p 2181:2181 \
        -p 8080:8080 \
        -e ZK_SERVER_ID=1 \
        -e ZK_NODE_1="zk-1:2182:2183:participant;2181" \
        -e ZK_NODE_2="zk-2:2182:2183:participant;2181" \
        -e ZK_NODE_3="zk-3:2182:2183:participant;2181" \
        $IMAGE_NAME
