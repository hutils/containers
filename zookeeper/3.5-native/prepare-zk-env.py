#!/usr/bin/python3

import argparse
import multiprocessing
import signal

import grp
import os
import socket
import subprocess

import pwd
from os import environ, path

import sys
from time import sleep

from jinja2 import Environment, FileSystemLoader


class KeyValueParserAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super(KeyValueParserAction, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        key, val = values.split('=')
        config_map = getattr(namespace, self.dest, dict())
        if config_map is None:
            config_map = dict()
        # map HBase config keys from dot-separated to underscore-separated(lowered snake case)
        # for instance, io.buffer.size into io_buffer_size
        # required by Jinja: dot have another meaning in Jinja variable name
        normalized_key = key.replace('.', '_').replace('-', '_').lower()
        config_map[normalized_key] = val
        setattr(namespace, self.dest, config_map)


def create_argparser():
    arg_parser = argparse.ArgumentParser(description='Zookeeper server')
    arg_parser.add_argument('-D',
                            action=KeyValueParserAction,
                            help="Zookeeper config option value in form 'key=value'",
                            nargs='*',
                            metavar='KEY=VALUE',
                            dest='config_map')
    return arg_parser


def get_template_env(template_dir):
    fs_loader = FileSystemLoader(searchpath=template_dir, followlinks=True)
    return Environment(autoescape=True, loader=fs_loader)


def create_config_templates(cmd_args):
    template_ctx = cmd_args.config_map
    if template_ctx is None:
        template_ctx = dict()

    uid = pwd.getpwnam(environ['ZK_USER']).pw_uid
    gid = grp.getgrnam(environ['ZK_GROUP']).gr_gid
    zk_conf_dir = environ['ZK_CONF_DIR']
    env = get_template_env(zk_conf_dir)

    zoo_cfg = env.get_template("zoo.cfg.j2")
    zoo_cfg_content = zoo_cfg.render(template_ctx)

    # skip dynamic config file if already exists in mounted volume
    config_files = [("zoo.cfg", zoo_cfg_content)]
    if not path.exists(environ['ZK_CONF_DIR'] + "/zoo.cfg.dynamic"):
        if not template_ctx['standalone']:
            zk_dynamic = env.get_template("zoo.cfg.dynamic.j2")
            zk_dynamic_content = zk_dynamic.render(template_ctx)
            config_files.append(("zoo.cfg.dynamic", zk_dynamic_content))
        else:
            print("Skip zoo.cfg.dynamic file creation because standalone mode enabled", flush=True)
    else:
        print("Skip zoo.cfg.dynamic file creation because already exists", flush=True)

    for name, content in config_files:
        print(flush=True)
        file_path = os.path.join(zk_conf_dir, name)
        file = open(file_path, mode='w', encoding='utf-8')
        file.write(content)
        file.close()
        file = open(file_path, mode='r', encoding='utf-8')
        print("Create config file " + file_path + ":", flush=True)
        print(file.read(), flush=True)
        file.close()
        os.chown(file_path, uid, gid)
        os.chmod(file_path, 0o755)

def set_sys_config_values(cmd_args):
    if cmd_args.config_map is None:
        cmd_args.config_map = dict()

    validate_and_overwrite_server_id()

    cmd_args.config_map['data_dir'] = environ['ZK_DATA_DIR']
    cmd_args.config_map['data_log_dir'] = environ['ZK_DATA_LOG_DIR']
    cmd_args.config_map['server_id'] = environ['ZK_SERVER_ID']
    cmd_args.config_map['nodes'] = read_node_list(cmd_args)


def validate_and_overwrite_server_id():
    if len(environ['ZK_SERVER_ID']) == 0:
        id_start_idx = socket.gethostname().rfind("-")
        id_end_idx = len(socket.gethostname())
        # if hostname contains dots(for instance zk-1.mydomain.com) try to extract server ID
        # between '-' and first dot. Otherwise if hostname doesn't contain domain part,
        # between '-' and end of hostname
        domain_path_start_idx = socket.gethostname().find(".")
        if domain_path_start_idx != -1 and domain_path_start_idx > id_start_idx:
            id_end_idx = domain_path_start_idx

        server_id = socket.gethostname()[id_start_idx + 1: id_end_idx]

        if id_start_idx == -1:
            print("Cannot infer Zookeeper server ID from hostname. "
                  "Set hostname in format 'hostname-<SERVER_ID>' or set env variable ZK_SERVER_ID",
                  flush=True)
            exit(os.EX_TEMPFAIL)
        try:
            zk_id = int(server_id) + 1
            environ['ZK_SERVER_ID'] = str(zk_id)
        except:
            print("Invalid Zookeeper server ID(must be digit, great than 0). "
                  "Set hostname in format 'hostname-<SERVER_ID>' or set env variable ZK_SERVER_ID",
                  flush=True)
            exit(os.EX_TEMPFAIL)

    if not environ['ZK_SERVER_ID'].isdigit():
        print("Invalid Zookeeper server ID(must be digit, great than 0). "
              "Set hostname in format 'hostname-<SERVER_ID>' or set env variable ZK_SERVER_ID",
              flush=True)
        exit(os.EX_TEMPFAIL)

    print("Current Zookeeper server myid=" + environ['ZK_SERVER_ID'], flush=True)


def read_node_list(cmd_args):
    node_list = {}
    prefix = "ZK_NODE_".lower()
    for key in os.environ.keys():
        if key.lower().startswith(prefix):
            start_idx = key.find(prefix) + len(prefix) + 1
            server_id_str = key[start_idx: len(key)]
            try:
                myid = int(server_id_str)
                node_record = "server." + server_id_str + "=" + environ[key]
                node_list[myid] = node_record
            except:
                print("Server ID(myid) has invalid value: " + server_id_str,
                      "\nNode record env variable: " + key + "=" + environ[key],
                      flush=True)
                exit(os.EX_TEMPFAIL)

    # if no dynamic config found in environment variables =>
    # create 1 record using current server myid
    if len(node_list) == 0:
        cmd_args.config_map['standalone'] = True
        cur_server_id = int(environ["ZK_SERVER_ID"])
        node_list[cur_server_id] = "server." + str(cur_server_id) \
                                   + "=0.0.0.0:2182:2183:participant;2181"
    else:
        cmd_args.config_map['standalone'] = False

    return node_list


def join_to_cluster():
    try:
        if int(environ['RECONFIG_DELAY']) <= 0:
            return
    except:
        return

    sleep(int(environ['RECONFIG_DELAY']))

    zk_cli_script = path.join(environ['ZK_INSTALLATION_DIR'], 'bin/zkCli.sh')
    cur_node_record = read_node_list()[int(environ['ZK_SERVER_ID'])]
    zk_cli_process = subprocess.Popen(['/bin/bash', zk_cli_script,
                                       "reconfig",
                                       "-add",
                                       cur_node_record])
    zk_cli_process.wait()
    if zk_cli_process.returncode != 0:
        exit(os.EX_TEMPFAIL)

# -------------Main script body----------------

arg_parser = create_argparser()
try:
    args = create_argparser().parse_args()
except:
    arg_parser.print_help()
    exit(os.EX_USAGE)

set_sys_config_values(args)
create_config_templates(args)
join_to_cluster()
