#!/usr/bin/env bash

#create ZK data/log dirs and set access rights on runtime to support Docker host volumes
mkdir -p $ZK_DATA_LOG_DIR \
         $ZK_DATA_LOG_DIR_V2 \
         $ZK_DATA_DIR \
         $ZK_DATA_DIR_V2 \
         $ZK_CONF_DIR \
         $ZK_LOG_DIR

chown -R $ZK_USER:$ZK_GROUP \
         $ZK_DATA_LOG_DIR \
         $ZK_DATA_DIR \
         $ZK_CONF_DIR \
         $ZK_INSTALLATION_DIR

chmod -R 750 \
         $ZK_DATA_LOG_DIR \
         $ZK_DATA_DIR \
         $ZK_CONF_DIR \
         $ZK_INSTALLATION_DIR

ln -s $ZK_CONF_DIR "/etc/zookeeper"

#create myid file
echo $ZK_SERVER_ID > $ZK_DATA_DIR/myid
chown $ZK_USER:$ZK_GROUP $ZK_DATA_DIR/myid
chmod 750 $ZK_DATA_DIR/myid

export ZK_JVMFLAGS="-Dzookeeper.datadir.autocreate=false \
                    -Dzookeeper.serverCnxnFactory=org.apache.zookeeper.server.NIOServerCnxnFactory \
                    -Dzookeeper.nio.numSelectorThreads=$NIO_NUM_SELECTOR_THREADS \
                    -Dzookeeper.nio.numWorkerThreads=$NUM_WORKER_THREADS \
                    -Dzookeeper.commitProcessor.numWorkerThreads=$COMMIT_PROCESSOR_NUM_WORKER_THREADS \
                    -Dzookeeper.logger.level=$ZK_LOG_LEVEL \
                    -Dfsync.warningthresholdms=$FSYNC_THRESHOLD \
                    -Dreadonlymode.enabled=$READONLYMODE_ENABLED \
                    -Dzookeeper.skipACL=$SKIP_ACL \
                    -Dznode.container.checkIntervalMs=$ZNODE_CONTAINER_CHECK_INTERVAL_MS \
                    -Dznode.container.maxPerMinute=$ZNODE_CONTAINER_MAX_PER_MINUTE \
                    -Dzookeeper.jmx.log4j.disable=true \
                    -Dzookeeper.leaderServes=$LEADER_SERVES \
                    -Djava.net.preferIPv4Stack=true \
                    -Dnetworkaddress.cache.negative.ttl=0 \
                    -Xmx${ZK_SERVER_MAX_HEAP} \
                    -Xms${ZK_SERVER_MIN_HEAP} \
                    $JVM_CUSTOM_OPTS"

ARGS=`echo ${@}`
python3 "/prepare-zk-env.py" $ARGS

echo "Starting Zookeeper server..."

gosu "$ZK_USER:$ZK_GROUP" \
     zookeeper-server ${ZK_JVMFLAGS} "$ZK_CONF_DIR/zoo.cfg"
