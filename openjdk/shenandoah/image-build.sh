#!/usr/bin/env bash

UBUNTU_VERSION=${1:-18.10}
JAVA_VERSION=${2:-11}

REGISTRY="hutils"
IMAGE_NAME="$REGISTRY/openjdk-shenandoah:$JAVA_VERSION"

sudo podman build --no-cache \
             --build-arg JAVA_VERSION=$JAVA_VERSION \
             --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
             -t $IMAGE_NAME .
if [[ $? != 0 ]]; then
    exit $?
fi
sudo podman push $IMAGE_NAME