#!/usr/bin/env bash

. ./env.sh

HBASE_VERSION_TO_BUILD=$1
HADOOP_VERSION=$2
OUT_DIR="$(pwd)/build-output"

#rm -Rf ${OUT_DIR}
mkdir -p ${OUT_DIR}
chmod 777 ${OUT_DIR}

sudo podman container run \
        -it \
        --rm \
        -v ${OUT_DIR}:/build-output \
        ${IMAGE_NAME} \
        ${HBASE_VERSION_TO_BUILD} \
        ${HADOOP_VERSION}
