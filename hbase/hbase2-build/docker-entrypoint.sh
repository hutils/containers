#!/usr/bin/env bash

HBASE_VERSION=$1
HADOOP_VERSION=$2
HADOOP_PROFILE=""
HBASE_BUILD_PARAMS="-Prelease -DskipTests -Dfindbugs.skip=true -Dcheckstyle.skip=true -Drat.skip=true"

if  [[ ${HADOOP_VERSION} == 3.* ]] ;
then
    HADOOP_PROFILE="${HADOOP_PROFILE} -Dhadoop.profile=3.0 -Dhadoop-three.version=$HADOOP_VERSION"
elif  [[ ${HADOOP_VERSION} == 2.* ]] ;
then
    HADOOP_PROFILE="${HADOOP_PROFILE} -Dhadoop-two.version=$HADOOP_VERSION"
else
    echo "Hadoop version(set in second parameter) must be 2.x.x or 3.x.x"
    exit -1
fi

if  [[ ${HBASE_VERSION} == 1.* ]] ;
then
    # to skip Maven enforcer rule that HBase 1.* should be build against JDK 1.7
    # Maven 3.6.0 cannot be used because bug in findbugs plugin(v3.0.0) used in HBase 1.*
    # TODO: may be install OpenJDK7(Apache builds use JDK7 and can be run on it)
    # TODO: and override JAVA_HOME and MAVEN_HOME(to 3.5.4) here?
    HBASE_BUILD_PARAMS="${HBASE_BUILD_PARAMS} -DcompileSource=1.8"
fi

# -------------------------------------------------------------------------------------

git clone ${GIT_URL}
cd hbase
git checkout rel/${HBASE_VERSION}

MAVEN_OPTS="-Xmx4g -XX:MaxPermSize=256m"
mvn clean install ${HADOOP_PROFILE} \
                  ${HBASE_BUILD_PARAMS}

RET_CODE=$?
if [[ ${RET_CODE} != 0 ]]; then
    exit ${RET_CODE}
fi
mvn install ${HADOOP_PROFILE} \
            ${HBASE_BUILD_PARAMS} \
            assembly:single

RET_CODE=$?
if [[ ${RET_CODE} != 0 ]]; then
    exit ${RET_CODE}
fi

HBASE_ASSEMBLY_DIR="$(pwd)/hbase-assembly/target"

echo "HBase assembly build directory:"
ls -l ${HBASE_ASSEMBLY_DIR}

echo "Append Hadoop native libs"
cd ${HBASE_ASSEMBLY_DIR}
echo "Download Hadoop native libs..."
wget -O hadoop-native-${HADOOP_VERSION}.tar.gz \
     https://bintray.com/hutils/hadoop/download_file?file_path=hadoop-native-${HADOOP_VERSION}.tar.gz

echo "Unpack HBase build tar"
tar xf hbase-${HBASE_VERSION}-bin.tar.gz

echo "Unpack HBase build tar"
mkdir -p hbase-${HBASE_VERSION}/lib/native/Linux-amd64-64
tar xf hadoop-native-${HADOOP_VERSION}.tar.gz \
    -C hbase-${HBASE_VERSION}/lib/native/Linux-amd64-64

echo "Remove outdated HBase build tar"
rm -f hbase-${HBASE_VERSION}-bin.tar.gz
echo "Pack updated HBase build tar with native libs"
tar -czvf hbase-${HBASE_VERSION}-bin.tar.gz hbase-${HBASE_VERSION}

cp -R hbase-${HBASE_VERSION}-bin.tar.gz \
      ${BUILD_OUTPUT}/hbase-${HBASE_VERSION}-hadoop-${HADOOP_VERSION}-bin.tar.gz

echo "HBase build output directory:"
ls -l ${BUILD_OUTPUT}
chmod 777 -R ${BUILD_OUTPUT}