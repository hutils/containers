#!/usr/bin/env bash

. ./env.sh

sudo podman build --no-cache -t ${IMAGE_NAME} .
sudo podman push ${IMAGE_NAME}