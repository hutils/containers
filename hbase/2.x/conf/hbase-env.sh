#!/usr/bin/env bash
#
#/**
# * Licensed to the Apache Software Foundation (ASF) under one
# * or more contributor license agreements.  See the NOTICE file
# * distributed with this work for additional information
# * regarding copyright ownership.  The ASF licenses this file
# * to you under the Apache License, Version 2.0 (the
# * "License"); you may not use this file except in compliance
# * with the License.  You may obtain a copy of the License at
# *
# *     http://www.apache.org/licenses/LICENSE-2.0
# *
# * Unless required by applicable law or agreed to in writing, software
# * distributed under the License is distributed on an "AS IS" BASIS,
# * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# * See the License for the specific language governing permissions and
# * limitations under the License.
# */

# Set environment variables here.

export STD_OPTS="-XX:MaxMetaspaceSize=$HBASE_METASPACESIZE \
                 $JVM_JIT_OPTS \
                 $JVM_PERF_OPTS \
                 $HBASE_GC_OPTS \
                 $JVM_MEMORY_OPTS \
                 $HBASE_TRACE_OPTS \
                 $HBASE_GC_TRACE_OPTS \
                 $HBASE_JMX_BASE \
                 $JVM_CUSTOM_OPTS \
                 -Dhbase.root.logger.level=${HBASE_ROOT_LOG_LEVEL}"

export HBASE_MASTER_OPTS="$HBASE_MASTER_OPTS $STD_OPTS"
if [[ ! -z ${HBASE_JMX_BASE} ]]; then
    HBASE_MASTER_OPTS="$HBASE_MASTER_OPTS
                       $HBASE_JMX_BASE
                       -Dcom.sun.management.jmxremote.port=$CONF_HBASE_MASTER_JMX_PORT
                       -Dcom.sun.management.jmxremote.rmi.port=$CONF_HBASE_MASTER_JMX_PORT"
fi

export HBASE_REGIONSERVER_OPTS="$HBASE_REGIONSERVER_OPTS $STD_OPTS"
if [[ ! -z ${HBASE_JMX_BASE} ]]; then
    HBASE_REGIONSERVER_OPTS="$HBASE_REGIONSERVER_OPTS
                             $HBASE_JMX_BASE
                             -Dcom.sun.management.jmxremote.port=$CONF_HBASE_REGIONSERVER_JMX_PORT
                             -Dcom.sun.management.jmxremote.rmi.port=$CONF_HBASE_REGIONSERVER_JMX_PORT"
fi

if [[ ! -z ${HBASE_JMX_BASE} ]]; then
    HBASE_ZOOKEEPER_OPTS="$HBASE_ZOOKEEPER_OPTS
                          $HBASE_JMX_BASE
                          -Dcom.sun.management.jmxremote.port=$CONF_HBASE_ZK_JMX_PORT
                          -Dcom.sun.management.jmxremote.rmi.port=$CONF_HBASE_ZK_JMX_PORT"
fi

if [[ ! -z ${HBASE_JMX_BASE} ]]; then
    HBASE_THRIFT_OPTS="$HBASE_THRIFT_OPTS
                       $HBASE_JMX_BASE
                       -Dcom.sun.management.jmxremote.port=$CONF_HBASE_THRIFTSERVER_JMX_PORT
                       -Dcom.sun.management.jmxremote.rmi.port=$CONF_HBASE_THRIFTSERVER_JMX_PORT"
fi

if [[ ! -z ${HBASE_JMX_BASE} ]]; then
    HBASE_REST_OPTS="$HBASE_REST_OPTS
                     $HBASE_JMX_BASE
                     -Dcom.sun.management.jmxremote.port=$CONF_HBASE_RESTSERVER_JMX_PORT
                     -Dcom.sun.management.jmxremote.rmi.port=$CONF_HBASE_RESTSERVER_JMX_PORT"
fi