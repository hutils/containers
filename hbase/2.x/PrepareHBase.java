import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

public class PrepareHBase {

  private static final String CUR_USER = System.getProperty("user.name");

  public static void main(String[] args) {
    System.out.println("Run HBase under user " + CUR_USER);

    var properties = System.getProperties();
    setDefaults(properties);

    for (Path confPath : List.of(HBaseEnv.hbaseSite, HBaseEnv.coreSite)) {
      try {
        generateConfig(confPath, properties);
      }
      catch (Exception e) {
        final StringWriter writer = new StringWriter()
                                            .append("Can't create ")
                                            .append(confPath.toString())
                                            .append(": ");
        e.printStackTrace(new PrintWriter(writer));
        fatalError(writer.toString());
      }
    }
  }

  private static void generateConfig(Path confFilePath, Properties properties) throws IOException {
    if (Files.exists(confFilePath)) {
      System.out.println(confFilePath + " already exists. Skipping creation.");
      return;
    }

    try (var configWriter = Files.newBufferedWriter(confFilePath,
                                                    StandardOpenOption.CREATE,
                                                    StandardOpenOption.WRITE)) {

      configWriter.write("<?xml version=\"1.0\"?>");
      configWriter.newLine();
      configWriter.write("<configuration>");
      configWriter.newLine();
      final Pattern filterPattern = Pattern.compile(
          "^(java\\.|user\\.|path\\.|file\\.|sun\\.|line\\.|jdk\\.|os\\.|awt\\.).*");
      for (var kv : properties.entrySet()) {
        if (filterPattern.matcher(kv.getKey().toString()).matches()) {
          continue;
        }
        writeString(kv.getKey().toString(), kv.getValue().toString(), configWriter);
      }
      configWriter.write("</configuration>");
    }

    printFile(confFilePath);
  }

  private static void setDefaults(Properties properties) {
    final Path rootDir = checkAngEnvDir("HBASE_ROOT_DIR");
    final Path localDataDir = checkAngEnvDir("HBASE_LOCAL_DATA_DIR");

    properties.setProperty("hbase.local.dir", localDataDir.resolve("local-data").toString());
    properties.setProperty("hbase.tmp.dir", localDataDir.resolve("tmp").toString());
    properties.setProperty("hbase.master.port", System.getenv("CONF_HBASE_MASTER_PORT"));
    properties.setProperty("hbase.master.info.bindAddress", System.getenv("CONF_HBASE_MASTER_UI_ADDRESS"));
    properties.setProperty("hbase.master.info.port", System.getenv("CONF_HBASE_MASTER_UI_PORT"));
    properties.setProperty("hbase.regionserver.port", System.getenv("CONF_HBASE_REGIONSERVER_PORT"));
    properties.setProperty("hbase.regionserver.info.bindAddress", System.getenv("CONF_HBASE_REGIONSERVER_UI_ADDRESS"));
    properties.setProperty("hbase.regionserver.info.port", System.getenv("CONF_HBASE_REGIONSERVER_UI_PORT"));
    properties.setProperty("hbase.regionserver.info.port.auto", "false");

    if (properties.get("dfs.replication") == null) {
      properties.setProperty("dfs.replication", "3");
    }
    if (properties.get("dfs.client.read.shortcircuit") == null) {
      properties.setProperty("dfs.client.read.shortcircuit", "true");
    }
    if (properties.get("dfs.domain.socket.path") == null) {
      properties.setProperty("dfs.domain.socket.path", "/var/run/hdfs");
    }
    if (properties.get("dfs.client.block.write.retries") == null) {
      properties.setProperty("dfs.client.block.write.retries", "3");
    }
    if (properties.get("ipc.client.connect.timeout") == null) {
      properties.setProperty("ipc.client.connect.timeout", "3000");
    }

    if (properties.get("hbase.cluster.distributed") == null) {
      properties.setProperty("hbase.cluster.distributed", "false");
    }
    else if(!Boolean.parseBoolean(properties.get("hbase.cluster.distributed").toString())) {
      properties.setProperty("hbase.rootdir", rootDir.toString());
      properties.setProperty("hbase.zookeeper.property.dataDir",
                             localDataDir.resolve("zookeeper").toString());
      // for non HDFS cluster
      properties.setProperty("hbase.unsafe.stream.capability.enforce", "false");
    }
    else {
      properties.setProperty("hbase.cluster.distributed", "true");
      properties.setProperty("hbase.unsafe.stream.capability.enforce", "true");
    }
  }

  private static Path checkAngEnvDir(String envVarName) {
    final String value = System.getenv(envVarName);
    if (value == null || value.isBlank()) {
      fatalError("Environment variable " + envVarName + " must be set");
    }

    final Path path = Paths.get(value);
    if (!Files.exists(path)) {
      fatalError(value + " not exists");
    }
    if (!Files.isDirectory(path)) {
      fatalError(value + " is not directory");
    }
    if (!Files.isWritable(path) || !Files.isReadable(path) || !Files.isExecutable(path)) {
      fatalError(value + " not accessible for user " + CUR_USER + ". Expected permissions is rwx.");
    }
    return path;
  }

  private static void writeString(String confKey, String value, BufferedWriter configWriter)
      throws IOException {

    configWriter.write("<property>");
    configWriter.newLine();
    configWriter.write("\t<name>");
    configWriter.write(confKey);
    configWriter.write("</name>");
    configWriter.newLine();
    configWriter.write("\t<value>");
    configWriter.write(value);
    configWriter.write("</value>");
    configWriter.newLine();
    configWriter.write("</property>");
    configWriter.newLine();
  }

  private static void printFile(Path conf) throws IOException {
    System.out.println(conf + " file:"
                       + System.lineSeparator()
                       + StandardCharsets.UTF_8.decode(ByteBuffer.wrap(Files.readAllBytes(conf))));
  }

  private static void fatalError(String error) {
    System.err.println("FATAL: " + error);
    System.exit(-1);
  }

  static class HBaseEnv {

    public static final Path confDir;
    public static final Path hbaseSite;
    public static final Path coreSite;

    static {
      confDir = Paths.get(System.getenv("HBASE_CONF_DIR"));
      hbaseSite = confDir.resolve("hbase-site.xml");
      coreSite = confDir.resolve("core-site.xml");
    }

  }
}