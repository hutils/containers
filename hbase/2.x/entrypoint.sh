#!/bin/bash

if [[ -z "${HBASE_ROOT_LOG_LEVEL}" ]]; then
    HBASE_ROOT_LOG_LEVEL='INFO'
fi

# skip if present, to support custom value defined by user
if [[ -z "${HBASE_ROOT_LOGGER}" ]]; then
    HBASE_ROOT_LOGGER="$HBASE_ROOT_LOG_LEVEL,asyncconsole"
fi

CONF_PARAMS=""
ARGS=""
for key in $@; do
    if [[ ${key} == -D* ]]; then
        CONF_PARAMS="$CONF_PARAMS $key"
    else
        ARGS="$ARGS $key"
    fi
done

${JAVA_HOME}/bin/java --source 11 ${CONF_PARAMS} /PrepareHBase.java
if [[ $? != 0 ]]; then
    exit $?
fi

exec ${HBASE_HOME}/bin/hbase ${ARGS}