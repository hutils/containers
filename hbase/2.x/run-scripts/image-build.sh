#!/usr/bin/env bash

HBASE_VERSION=$1
HADOOP_VERSION=$2
. ./env.sh

cd ..

sudo podman build --no-cache \
             --build-arg HBASE_VERSION=${HBASE_VERSION} \
             --build-arg HADOOP_VERSION=${HADOOP_VERSION} \
             -t ${HBASE_IMAGE_NAME} .

if [[ $? != 0 ]]; then
    exit $?
fi

sudo podman push ${HBASE_IMAGE_NAME}