#!/bin/bash

check_result() {
  if [[ $? != 0 ]]; then
    exit $?
  fi
}

. ./env.sh

POD_NAME="hbaseCluster"
echo Remove pod ${POD_NAME}
sudo podman pod kill ${POD_NAME}
sudo podman pod rm -f ${POD_NAME}

HADOOP_VERSION="2.8.5"
HADOOP_IMAGE_NAME="$REGISTRY/hadoop-hdfs:${HADOOP_VERSION}"
ZK_VERSION="3.5.4-beta"
ZK_IMAGE_NAME="$REGISTRY/zookeeper:${ZK_VERSION}"
ZK_QUORUM="localhost:2181,localhost:2182,localhost:2183"
ZN_NODE="/hbase"

echo Create pod ${POD_NAME}
sudo podman pod create --name ${POD_NAME} --share=net \
            -p 2181:2181 \
            -p 2182:2182 \
            -p 2183:2183 \
            -p 16000:16000 \
            -p 16001:16001 \
            -p 16002:16002 \
            -p 16010:16010 \
            -p 16011:16011 \
            -p 16012:16012 \
            -p 16020:16020 \
            -p 16021:16021 \
            -p 16022:16022 \
            -p 16030:16030 \
            -p 16031:16031 \
            -p 16032:16032 \
            -p 8020:8020 \
            -p 50070:50070 \
            -p 50010:50010 \
            -p 50011:50011 \
            -p 50012:50012 \
            -p 50020:50020 \
            -p 50021:50021 \
            -p 50022:50022 \
            -p 50080:50080 \
            -p 50081:50081 \
            -p 50082:50082 \

check_result

for ID in 1 2 3
do
    NAME="zk-${ID}"
    echo Add zk container ${NAME}
    sudo podman create \
        --name ${NAME} \
        --hostname localhost \
        --pod ${POD_NAME} \
        -e ZK_SERVER_ID=${ID} \
        -e JMXDISABLE="true" \
        -e ZK_NODE_1="localhost:2191:2171:participant;2181" \
        -e ZK_NODE_2="localhost:2192:2172:participant;2182" \
        -e ZK_NODE_3="localhost:2193:2173:participant;2183" \
        ${ZK_IMAGE_NAME} \
        server

        check_result
done

DN_NN_COMMON_OPTS="-Ddfs.blocksize=128m -Ddfs.replication=3"
NN_OPTS="$DN_NN_COMMON_OPTS \
                -Ddfs.namenode.rpc-address=0.0.0.0:8020 \
                -Ddfs.namenode.http-address=0.0.0.0:50070 \
                -Ddfs.namenode.resource.du.reserved=5368709120 \
                -Ddfs.namenode.replication.min=1 \
                -Ddfs.namenode.handler.count=20 \
                -Ddfs.namenode.safemode.threshold-pct=1 \
                -Ddfs.namenode.safemode.min.datanodes=1 \
                -Ddfs.namenode.resource.checked.volumes.minimum=1 \
                -Ddfs.namenode.safemode.extension=1000 \
                -Ddfs.namenode.support.allow.format=true \
                -Ddfs.namenode.avoid.read.stale.datanode=true \
                -Ddfs.namenode.avoid.write.stale.datanode=true \
                -Ddfs.webhdfs.enabled=true \
                -Ddfs.image.compress=true \
                -Ddfs.image.compression.codec=org.apache.hadoop.io.compress.SnappyCodec"

echo Add Namenode container
sudo podman create \
        --name namenode \
        --hostname localhost \
        --pod ${POD_NAME} \
        -e CHECK_OR_RUN_HDFS_FORMAT=true \
        ${HADOOP_IMAGE_NAME} \
        ${NN_OPTS} \
        namenode

for ID in 0 1 2
do
    export DN_OPTS="-Ddfs.namenode.rpc-address=localhost:8020 \
                    $DN_NN_COMMON_OPTS \
                    -Ddfs.datanode.address=0.0.0.0:5001${ID} \
                    -Ddfs.datanode.http.address=0.0.0.0:5008${ID} \
                    -Ddfs.datanode.ipc.address=0.0.0.0:5002${ID} \
                    -Ddfs.heartbeat.interval=3 \
                    -Ddfs.datanode.du.reserved=1073741824 \
                    -Ddfs.datanode.du.reserved=1073741824 \
                    -Ddfs.datanode.balance.bandwidthpersec=1048576 \
                    -Ddfs.datanode.directoryscan.threads=2 \
                    -Ddfs.datanode.failed.volumes.tolerated=0"

    NAME="datanode-${ID}"
    echo Add datanode container ${NAME}
    sudo podman create \
            --name ${NAME} \
            --hostname localhost \
            --pod ${POD_NAME} \
            ${HADOOP_IMAGE_NAME} \
            ${DN_OPTS} \
            datanode
done

echo Start HDFS and Zookeeper
sudo podman pod start ${POD_NAME}

printf 'Wait HDFS start'
until $(curl --output /dev/null --silent --head --fail http://localhost:50070); do
    printf '.'
    sleep 1
done

echo Wait safemode exit...
sudo podman run \
        --name safemode-wait \
        --hostname localhost \
        --pod ${POD_NAME} \
        -it \
        --rm \
        ${HADOOP_IMAGE_NAME} \
        "-Dipc.client.connect.max.retries=100" \
        "-Dfs.defaultFS=hdfs://localhost:8020" \
        dfsadmin -safemode wait

sudo podman run \
        --name mkdir \
        --hostname localhost \
        --pod ${POD_NAME} \
        -it \
        --rm \
        ${HADOOP_IMAGE_NAME} \
        "-Dipc.client.connect.max.retries=100" \
        "-Dfs.defaultFS=hdfs://localhost:8020" \
        dfs -mkdir -p /hbase

sudo podman run \
        --name chmod \
        --hostname localhost \
        --pod ${POD_NAME} \
        -it \
        --rm \
        ${HADOOP_IMAGE_NAME} \
        "-Dipc.client.connect.max.retries=100" \
        "-Dfs.defaultFS=hdfs://localhost:8020" \
        dfs -chmod -R 777 /

HBASE_ARGS="-Dhbase.cluster.distributed=true \
            -Dhbase.zookeeper.quorum=${ZK_QUORUM} \
            -Dzookeeper.znode.parent=${ZN_NODE} \
            -Dhbase.rootdir=hdfs://localhost:8020/hbase \
            -Dhbase.bucketcache.ioengine=offheap \
            -Dhbase.bucketcache.size=128 \
            -Ddfs.replication=3 \
            -Ddfs.blocksize=128m"

for ID in 0 1 2
do
NAME="hbase-master-${ID}"
echo "Start HBase master container ${NAME}"
sudo podman create \
        --name hbase-master-"${ID}" \
        --hostname localhost \
        --pod ${POD_NAME} \
        -e HBASE_ROOT_LOG_LEVEL="DEBUG" \
        -e HBASE_JMX_BASE="" \
        -e CONF_HBASE_MASTER_PORT=1600"${ID}" \
        -e CONF_HBASE_MASTER_UI_PORT=1601"${ID}" \
        ${HBASE_IMAGE_NAME} \
        master start \
        -Dhbase.split.wal.zk.coordinated=false \
        -Dhbase.balancer.tablesOnMaster=true \
        -Dhbase.balancer.tablesOnMaster.systemTablesOnly=true \
        ${HBASE_ARGS}

        check_result
        sudo podman start hbase-master-"${ID}"
        check_result
done

for ID in 0 1 2
do
    NAME="hbase-region-${ID}"
    echo Start HBase region server container ${NAME}
	sudo podman create \
        --name hbase-region-"$ID" \
        --hostname localhost \
        --pod ${POD_NAME} \
        -e HBASE_JMX_BASE="" \
        -e CONF_HBASE_REGIONSERVER_PORT=1602"$ID" \
        -e CONF_HBASE_REGIONSERVER_UI_PORT=1603"$ID" \
        ${HBASE_IMAGE_NAME} \
        regionserver start \
        ${HBASE_ARGS}

        check_result
        sudo podman start hbase-region-"${ID}"
        check_result
done

check_result
sudo podman logs -f hbase-master-0