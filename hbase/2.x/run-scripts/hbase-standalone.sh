#!/usr/bin/env bash

HBASE_VERSION=$1
HADOOP_VERSION=$2
. ./env.sh

ZN_NODE="/hbase"
ZK_QUORUM="localhost:2181"

sudo podman pod rm -f hbase
sudo podman pod create --name hbase --share=net \
            -p 2181:2181 \
            -p 16000:16000 \
            -p 16010:16010 \
            -p 16020:16020 \
            -p 16030:16030

sudo podman create \
        --name hbase-master \
        --hostname localhost \
        -it \
        --rm \
        --pod hbase \
        -e HBASE_HEAPSIZE="8G" \
        -e HBASE_OFFHEAPSIZE="5G" \
        ${HBASE_IMAGE_NAME} \
        master start \
        -Dhbase.split.wal.zk.coordinated=false \
        -Dhbase.bucketcache.ioengine=offheap \
        -Dhbase.bucketcache.size=128 \
        -Dhbase.zookeeper.quorum=${ZK_QUORUM} \
        -Dzookeeper.znode.parent=${ZN_NODE}

#sudo podman create \
#        --name hbase-pe \
#        -it \
#        --rm \
#        --pod hbase \
#        ${IMAGE_NAME} \
#        -Dhbase.zookeeper.quorum=${ZK_QUORUM} \
#        -Dzookeeper.znode.parent=${ZN_NODE} \
#        pe \
#        --nomapred \
#        --rows=1500000 \
#        --valueSize=8192 \
#        --valueZipf=true \
#        --table=testTable \
#        --columns=5 \
#        --compress=SNAPPY \
#        --families=3 \
#        --presplit=10 \
#        randomWrite \
#        10


sudo podman pod start hbase
sudo podman attach hbase-master
#sudo podman attach hbase-pe
