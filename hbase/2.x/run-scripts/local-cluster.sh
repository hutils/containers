#!/bin/bash

check_result() {
  if [[ $? != 0 ]]; then
    exit $?
  fi
}

. ./env.sh

POD_NAME="hbaseCluster"
echo Remove pod ${POD_NAME}
sudo podman pod kill ${POD_NAME}
sudo podman pod rm -f ${POD_NAME}

# FS_PATH="/tmp/hbase/${POD_NAME}"
FS_PATH="/home/lagrang/Downloads/hbaseData/${POD_NAME}"
# sudo rm -Rf ${FS_PATH}
sudo mkdir -p ${FS_PATH}
sudo chmod -R 777 ${FS_PATH}

ZK_IMAGE_NAME="$REGISTRY/zookeeper:3.5.4-beta"
ZK_QUORUM="localhost:2181,localhost:2182,localhost:2183"
ZN_NODE="/hbase"

HBASE_ARGS="-Dhbase.cluster.distributed=true \
            -Dhbase.zookeeper.quorum=${ZK_QUORUM} \
            -Dzookeeper.znode.parent=${ZN_NODE} \
            -Dhbase.rootdir=/var/lib/hbase/root-dir \
            -Dhbase.bucketcache.ioengine=offheap \
            -Dhbase.bucketcache.size=128 \
            -Ddfs.replication=3"

echo Create pod ${POD_NAME}
sudo podman pod create --name ${POD_NAME} --share=net \
            -p 8081:8080 \
            -p 2181:2181 \
            -p 2182:2182 \
            -p 2183:2183 \
            -p 16000:16000 \
            -p 16001:16001 \
            -p 16002:16002 \
            -p 16010:16010 \
            -p 16011:16011 \
            -p 16012:16012 \
            -p 16020:16020 \
            -p 16021:16021 \
            -p 16022:16022 \
            -p 16030:16030 \
            -p 16031:16031 \
            -p 16032:16032

check_result

for ID in 1 2 3
do
    NAME="zk-${ID}"
    echo Add zk container ${NAME}
    sudo podman create \
        --name ${NAME} \
        --hostname localhost \
        --pod ${POD_NAME} \
        -e ZK_SERVER_ID=${ID} \
        -e JMXDISABLE="true" \
        -e ZK_NODE_1="localhost:2191:2171:participant;2181" \
        -e ZK_NODE_2="localhost:2192:2172:participant;2182" \
        -e ZK_NODE_3="localhost:2193:2173:participant;2183" \
        ${ZK_IMAGE_NAME} \
        server

        check_result
done

for ID in 0 1 2
do
NAME="hbase-master-${ID}"
echo "Add hbase master container ${NAME}"
sudo podman create \
        --name hbase-master-"${ID}" \
        --hostname localhost \
        --pod ${POD_NAME} \
        -v ${FS_PATH}:/var/lib/hbase/root-dir \
        -e HBASE_ROOT_LOG_LEVEL="INFO" \
        -e HBASE_JMX_BASE="" \
        -e CONF_HBASE_MASTER_PORT=1600"${ID}" \
        -e CONF_HBASE_MASTER_UI_PORT=1601"${ID}" \
        ${HBASE_IMAGE_NAME} \
        master start \
        -Dhbase.split.wal.zk.coordinated=false \
        -Dhbase.balancer.tablesOnMaster=true \
        -Dhbase.balancer.tablesOnMaster.systemTablesOnly=true \
        ${HBASE_ARGS}

        check_result
done

for ID in 0 1 2
do
    NAME="hbase-region-${ID}"
    echo Add hbase region server container ${NAME}
	sudo podman create \
        --name hbase-region-"$ID" \
        --hostname localhost \
        --pod ${POD_NAME} \
        -v ${FS_PATH}:/var/lib/hbase/root-dir \
        -e HBASE_JMX_BASE="" \
        -e CONF_HBASE_REGIONSERVER_PORT=1602"$ID" \
        -e CONF_HBASE_REGIONSERVER_UI_PORT=1603"$ID" \
        ${HBASE_IMAGE_NAME} \
        regionserver start \
        ${HBASE_ARGS}

        check_result
done

# sudo podman create \
#         --name hadmin \
#         --pod ${POD_NAME} \
#         -e HTTP_BIND_ADDR="0.0.0.0" \
#         hutils/hadmin:0.2 \
#         ${ZK_QUORUM} \
#         ${ZK_NODE}

# sudo podman create \
#         --name hbase-pe \
#         -it \
#         --rm \
#         --pod ${POD_NAME} \
#         ${HBASE_IMAGE_NAME} \
#         pe \
#         -Dhbase.zookeeper.quorum=${ZK_QUORUM} \
#         -Dzookeeper.znode.parent=${ZK_NODE} \
#         --nomapred \
#         --rows=150000 \
#         --valueSize=8192 \
#         --valueZipf=true \
#         --table=testTable \
#         --columns=500 \
#         --compress=SNAPPY \
#         --families=3 \
#         --presplit=10 \
#         randomWrite \
#         5

echo Start pod ${POD_NAME}
sudo podman pod start ${POD_NAME}
check_result
#sudo podman attach zk-1
#sudo podman logs zk-1